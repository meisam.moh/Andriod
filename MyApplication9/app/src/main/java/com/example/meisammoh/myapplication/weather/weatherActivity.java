package com.example.meisammoh.myapplication.weather;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.meisammoh.myapplication.R;
import com.example.meisammoh.myapplication.weather.models.YhooModel;
import com.github.ybq.android.spinkit.style.DoubleBounce;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.roger.catloadinglibrary.CatLoadingView;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class weatherActivity extends AppCompatActivity implements View.OnClickListener {


    Context mContext = this;
    EditText e1;
    Button ok;
    TextView  result;
    ListView list;

    CatLoadingView mView;

    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        mView = new CatLoadingView();


        list=(ListView) findViewById(R.id.list);
        e1=(EditText)findViewById(R.id.e1);
        ok=(Button)findViewById(R.id.ok);
        result=(TextView) findViewById(R.id.result);
        ok.setOnClickListener(this);

      //  dialog = new ProgressDialog(mContext);
   //     dialog.setTitle("waiting");
     //   dialog.setCancelable(false);
    //    dialog.setMessage("Please wait for server response");


    }

    @Override
    public void onClick(View view) {
        mView.show(getSupportFragmentManager(), "logginnnnnn");
        String city=e1.getText().toString();
        if (!city.equals("")) {

           getdatayahoo(city);

//            dialog.onStart();
        }else{
            Toast.makeText(mContext, "null", Toast.LENGTH_SHORT).show();
            e1.setText("");
        }

    }




    void getdatayahoo(String city){
//         dialog.onStart();
         String url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22" + city + "%2C%20ir%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";

         AsyncHttpClient client = new AsyncHttpClient();
         client.get(url, new TextHttpResponseHandler() {


             @Override
             public void onStart() {
                 super.onStart();
//                 dialog.show();
             }

             @Override
             public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                 Toast.makeText(mContext, "error" + throwable, Toast.LENGTH_SHORT).show();
             }

             @Override
             public void onSuccess(int statusCode, Header[] headers, String responseString) {
                 showdata(responseString);
             }


             @Override
             public void onFinish() {
                 super.onFinish();
              //   dialog.dismiss();

             }
         });
    }
    void showdata(String responseString){

try {
    Gson Gson = new Gson();
    YhooModel yhooModel = Gson.fromJson(responseString, YhooModel.class);
    String temp = yhooModel.getQuery().getResults().getChannel().getItem().getCondition().getTemp();

    WeatherAdapter adapter = new WeatherAdapter(mContext,
            yhooModel.getQuery().getResults().getChannel().getItem().getForecast()) ;

    list.setAdapter(adapter);

    result.setText(temp);
    mView.dismiss();
} catch (Exception e){
    Toast.makeText(mContext, "City not fond", Toast.LENGTH_SHORT).show();
}



    //  try{
       //   JSONObject allobject =new JSONObject(responseString);
    //       String query=allobject.getString("query");
    //       JSONObject queryobject=new JSONObject(query);
//
 //     }catch (Exception e){

 //     }










    }



}
