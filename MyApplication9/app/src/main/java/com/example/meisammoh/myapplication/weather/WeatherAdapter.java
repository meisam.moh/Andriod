package com.example.meisammoh.myapplication.weather;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.meisammoh.myapplication.R;
import com.example.meisammoh.myapplication.weather.models.Forecast;
import com.squareup.picasso.Picasso;

import java.util.List;

public class WeatherAdapter extends BaseAdapter {
    Context mContext;
    List<Forecast> forecasts;

    public WeatherAdapter(Context mContext, List<Forecast> forecasts) {
        this.mContext = mContext;
        this.forecasts = forecasts;
    }

    @Override
    public int getCount() {
        return forecasts.size();
    }

    @Override
    public Object getItem(int i) {
        return forecasts.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View row = LayoutInflater.from(mContext).inflate(R.layout.forecat_item_list, viewGroup, false);

        TextView day1 = (TextView) row.findViewById(R.id.day1);
        TextView high1 = (TextView) row.findViewById(R.id.high1);
        TextView low1 = (TextView) row.findViewById(R.id.low1);
        TextView text1 = (TextView) row.findViewById(R.id.text1);
        ImageView icon1 = (ImageView) row.findViewById(R.id.icon1);

        day1.setText(forecasts.get(i).getDay());
        high1.setText(forecasts.get(i).getHigh());
        low1.setText(forecasts.get(i).getLow());
        text1.setText(forecasts.get(i).getText());


        String  code=forecasts.get(i).getCode();
        String  url="http://l.yimg.com/a/i/us/we/52/"+ code +".gif";
        Picasso.get().load(url).into(icon1);
        return row;
    }
}
