package com.example.meisammoh.myapplication.weather;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.meisammoh.myapplication.R;
import com.roger.catloadinglibrary.CatLoadingView;

public class LoadingActivity extends AppCompatActivity implements View.OnClickListener {

    Button  ok;
    CatLoadingView mView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);
       ok=(Button)findViewById(R.id.ok);
       ok.setOnClickListener(this);
        mView = new CatLoadingView();


    }

    @Override
    public void onClick(View view) {
        mView.show(getSupportFragmentManager(), "logginnnnnn");
    }
}
